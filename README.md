# Nixvim template

This template gives you a good starting point for configuring nixvim standalone.

## Configuring

To start configuring, just add or modify the nix files in `./config`.
If you add a new configuration file, remember to add it to the
[`config/default.nix`](./config/default.nix) file

## Testing your new configuration

To test your configuration simply run the following command

```
nix run .
```

# TODOS

- [ ] add toggleterm
- [x] add dial
- [ ] add code-runner
- [x] add winbar plugin or config
- [x] add dap
- [ ] add unittest
- [ ] add session
- [x] add nav-buddy
- [ ] add telescope symbols
- [x] add mini brackted
- [ ] add comment box
- [ ] add git hunk as per lazyvim?
- [ ] add notifications
- [x] add text-case.nvim
- [ ] add debugprint
- [x] add dashboard
- [x] add copilot
- [ ] add chatgpt
- [ ] add markdown styling
- [x] remove bufferline
- [ ] add keymaps for managing windows
- [ ] test on a python project
- [ ] test on a ts project
- [ ] add ts proper tool? ts-server or typescript-tool?
