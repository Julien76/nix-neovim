{
  pkgs,
  config,
  lib,
  ...
}: let
  inherit (lib) mkOption types;
in {
  imports = [
    # keymaps ---------------------------------
    ./keymaps/default.nix
    ./keymaps/editor.nix
    ./keymaps/buffer.nix
    ./keymaps/window.nix
    ./keymaps/utils.nix
    ./keymaps/diagnostics.nix
    # Autocommands ----------------------------
    ./autocommands.nix
    # Highlights ------------------------------
    ./highlights.nix
    # Plugins ---------------------------------
    ./plugins/copilot.nix
    ./plugins/flash.nix
    ./plugins/completion.nix
    ./plugins/comment-box.nix
    ./plugins/treesitter.nix
    ./plugins/utils.nix
    ./plugins/dashboard.nix
    ./plugins/lazygit.nix
    ./plugins/ui.nix
    ./plugins/git.nix
    ./plugins/lsp.nix
    ./plugins/neo-tree.nix
    ./plugins/lint.nix
    ./plugins/formatter.nix
    ./plugins/telescope.nix
    ./plugins/trouble.nix
    ./plugins/zen-mode.nix
    ./plugins/mini.nix
    # ./plugins/harpoon.nix
    ./plugins/dial.nix
    ./plugins/spectre.nix
    ./plugins/text-case.nix
    ./plugins/neotest.nix
    ./plugins/dap.nix
    ./plugins/toggleterm.nix
    # ── Plugins not yet in nixpkgs ────────────────────────────────────────
    ./plugins/extra-plugins.nix
  ];
  options = {
    theme = mkOption {
      type = types.enum ["everforest" "catppuccin" "gruvbox" "vscode" "nord" "nightfox"];
      default = "everforest";
      description = "Default global theme";
    };
  };

  config = {
    vimAlias = true;
    viAlias = true;

    globals = {
      mapleader = " ";
      # auto_save = 1;
      # auto_save_events = ["InsertLeave" "TextChanged"];
    };

    opts = {
      number = true;
      #colorcolumn = "80";
      relativenumber = false;
      shiftwidth = 2;
      tabstop = 2;
      wrap = false;
      swapfile = false; #Undotree
      backup = false; #Undotree
      undofile = true;
      hlsearch = false;
      incsearch = true;
      termguicolors = true;
      scrolloff = 8;
      signcolumn = "yes";
      updatetime = 200;
      foldlevelstart = 99;
      spell = false;
      spelllang = "en";
      # timeout = true;
      timeoutlen = 500;
      cursorline = true;
      splitbelow = true;
      splitright = true;
      ignorecase = true;
      smoothscroll = true;
      wildmode = "longest:full,full"; ## Command-line completion mode
      pumblend = 10; ## Popup blend
      pumheight = 10; ## Maximum number of entries in a popup
      title = true;
      titlestring = "neovim";
      sessionoptions = "buffers,curdir,folds,help,tabpages,winsize,winpos,terminal,localoptions";
      fillchars.eob = " ";
    };

    clipboard = {
      register = "unnamedplus";
      # TODO: Make conditional if X11/Wayland enabled
      providers.wl-copy.enable = true;
      #providers.xclip.enable = true;
      #providers.xsel.enable = false;
    };
    # Apply the dynamically selected colorschemes
    # colorschemes = colorschemes;
    # inherit colorschemes;
    colorschemes = {
      catppuccin = {
        enable = config.theme == "catppuccin";
        settings.flavour = "mocha";
      };
      gruvbox.enable = config.theme == "gruvbox";
      vscode.enable = config.theme == "vscode";
      nord.enable = config.theme == "nord";
      everforest = {
        enable = config.theme == "everforest";
        settings.background = "hard";
      };
      nightfox = {
        enable = config.theme == "nightfox";
        flavor = "nightfox";
      };
    };

    extraPlugins = with pkgs.vimPlugins; [
      {
        plugin = dressing-nvim;
        config = ''lua require('dressing').setup()'';
      }
      nvim-nio
      telescope-fzf-native-nvim
      # ── Extra colorschemes ────────────────────────────────────────────────
      no-clown-fiesta-nvim
      nightfox-nvim
      poimandres-nvim
    ];

    extraPackages = with pkgs; [
      # Formatters
      alejandra
      asmfmt
      cmake-format
      nodePackages.prettier
      prettierd
      shfmt
      stylua
      # Linters
      commitlint
      eslint_d
      hadolint
      html-tidy
      luajitPackages.luacheck
      markdownlint-cli
      nodePackages.jsonlint
      pylint
      pyright
      ruff
      nil
      mypy
      shellcheck
      yamllint
      statix
      # Debuggers / misc deps
      asm-lsp
      bashdb
      clang-tools
      delve
      fd
      gdb
      lldb_17
      llvmPackages_17.bintools-unwrapped
      marksman

      zlib
      stdenv.cc.cc.lib
      glibc
      libgcc
      gcc13Stdenv
      # (nerdfonts.override {
      #   fonts = [
      #     "JetBrainsMono"
      #     "RobotoMono"
      #   ];
      # })

      python3
      ripgrep
      rr
      tmux-sessionizer
    ];
  };
}
