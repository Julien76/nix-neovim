let
  helpers.mkRaw = r: {__raw = r;};
in {
  keymaps = [
    {
      mode = "n";
      key = "<leader>ey";
      action = helpers.mkRaw "function() require('yazi').yazi() end";
      options = {
        desc = "[E]xplore via [Y]azi";
      };
    }
  ];
}
# map('n', '<leader>hR', gitsigns.reset_buffer)

