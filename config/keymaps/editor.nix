#          ╭──────────────────────────────────────────────────────────╮
#          │ bindings for the editior / code                          │
#          ╰──────────────────────────────────────────────────────────╯
let
  helpers.mkRaw = r: {__raw = r;};
in {
  keymaps = [
    {
      key = "<leader>c";
      action = "<leader>c";
      options = {
        desc = "[C]ode";
        remap = true;
      };
    }
  ];
}
