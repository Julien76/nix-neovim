{
  keymaps = [
    {
      key = "<leader>w";
      action = "<leader>w";
      options = {
        desc = "[W]indow";
        remap = true;
      };
    }
    {
      mode = "n";
      key = "<leader>w-";
      action = "<cmd>split<cr>";
      options.desc = "[W]indow split horizontally";
    }
    {
      mode = "n";
      key = "<leader>w/";
      action = "<cmd>vsplit<cr>";
      options.desc = "[W]indow split vertically";
    }
    {
      mode = "n";
      key = "<leader>wh";
      action = "<cmd>split<cr>";
      options.desc = "[W]indow split [H]orizontally";
    }
    {
      mode = "n";
      key = "<leader>wv";
      action = "<cmd>vsplit<cr>";
      options.desc = "[W]indow split [V]ertically";
    }
    {
      mode = "n";
      key = "<leader>wd";
      action = "<cmd>close<cr>";
      options.desc = "[W]indow [C]lose";
    }
  ];
}
