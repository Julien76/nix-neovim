let
  helpers.mkRaw = r: {__raw = r;};
in {
  extraConfigLuaPre = ''
    local diagnostic_goto = function(next, severity)
      local go = next and vim.diagnostic.goto_next or vim.diagnostic.goto_prev
      severity = severity and vim.diagnostic.severity[severity] or nil
      return function()
        go({ severity = severity })
      end
    end
  '';

  keymaps = [
    {
      key = "]d";
      action = helpers.mkRaw "diagnostic_goto(true)";
      mode = "n";
      options.desc = "Next Diagnostic";
    }
    {
      key = "[d";
      action = helpers.mkRaw "diagnostic_goto(false)";
      mode = "n";
      options.desc = "Prev Diagnostic";
    }
    {
      key = "]e";
      action = helpers.mkRaw "diagnostic_goto(true, 'ERROR')";
      mode = "n";
      options.desc = "Next Error";
    }
    {
      key = "[e";
      action = helpers.mkRaw "diagnostic_goto(false, 'ERROR')";
      mode = "n";
      options.desc = "Prev Error";
    }
    {
      key = "]w";
      action = helpers.mkRaw "diagnostic_goto(true, 'WARN')";
      mode = "n";
      options.desc = "Next Warning";
    }
    {
      key = "[w";
      action = helpers.mkRaw "diagnostic_goto(false, 'WARN')";
      mode = "n";
      options.desc = "Prev Warning";
    }
  ];
}
