let
  helpers.mkRaw = r: {__raw = r;};
in {
  keymaps = [
    {
      key = "<leader>b";
      action = "<leader>b";
      options = {
        desc = "[B]uffer";
        remap = true;
      };
    }
    {
      key = "<leader>bb";
      action = "<cmd>b#<CR>";
      options.desc = "[B]uffer [B]ack (previous buffer)";
    }
    {
      key = "<leader>bd";
      action = helpers.mkRaw "function() MiniBufremove.delete(0) end";
      options.desc = "[B]uffer [D]elete";
    }
    {
      key = "<leader>bc";
      action = helpers.mkRaw "function() MiniBufremove.delete(0) end";
      options.desc = "[B]uffer [C]lose (same as delete)";
    }
  ];
}
