from datetime import datetime

stmt = """ -- sql
    SELECT *
    FROM my_table t
    WHERE t.id = '123'
"""

query = text(""" -- sql
    SELECT r.*
    FROM revision r
    JOIN phase p ON r.phase_id = p.phase_id
    WHERE r.phase_id = :phase_id
        AND p.is_deleted = FALSE
        AND r.is_deleted = FALSE;
""")


class MyClass:
    def __init__(self):
        self.a: int = 10

    def mmm(self):
        stmt = """ -- sql
            SELECT *
            FROM my_table t
            WHERE t.id = '123'
        """
        return self.a

    def mmm3(self) -> None:
        return self.a

    def mmm4(self, b, a, c):
        print("aaaa")
        return self.a + a + b

    def mmm5(self, b: int, a: float, c: int) -> int:
        print("aaaa")
        return self.a + int(a) + b


class TestMyClass:
    def __init__(self):
        pass


a = MyClass()
a.a = 2
a.mmm4()
b = a.mmm5(a=2.8, b=3, c=4)
print(a.a)
c = datetime.now()
a.mmm3()
a.mmm()
a.__dir__
