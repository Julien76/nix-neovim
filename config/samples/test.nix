{pkgs, ...}: {
  environment.shellAliases = [];

  environment.defaultPackages = [
    pkgs.cowsay
  ];
}
