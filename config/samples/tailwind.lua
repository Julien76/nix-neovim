tailwindcss = {
	filetypes = {
		"css",
		"scss",
		"sass",
		"html",
		"javascript",
		"javascriptreact",
		"typescript",
		"typescriptreact",
		"htmldjango",
		"python",
	},
	init_options = {
		userLanguages = {
			python = "html",
		},
	},
	settings = {
		includeLanguages = {
			typescript = "javascript",
			typescriptreact = "javascript",
			htmldjango = "html",
			python = "html",
		},
		tailwindCSS = {
			experimental = {
				classRegex = {
					-- Single line strings:
					[[class_="([^"]*)]], -- for double quoted strings
					[[class_='([^']*)]], -- for single quoted strings

					-- Multi-line strings:
					[[class_="""([^"]*)"""]], -- for multi-line double quoted strings
					[[class_='''([^']*)''']], -- for multi-line single quoted strings
				},
			},
		},
	},
}
