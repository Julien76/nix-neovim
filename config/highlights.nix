{
  highlight = {
    # CmpItemAbbr = { fg = colors.white; };
    # CmpItemAbbrMatch = { fg = colors.blue; bold = true; };
    # CmpDoc = { bg = colors.black2; };
    # CmpDocBorder = { fg = colors.black2; bg = colors.black2; };
    # CmpPmenu = { bg = colors.darker_black; };
    CmpSel = {
      link = "PmenuSel";
      bold = true;
    };
    # CmpBorder = { fg = colors.darker_black; bg = colors.darker_black; };
  };
}
