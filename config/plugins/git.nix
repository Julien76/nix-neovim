let
  helpers.mkRaw = r: {__raw = r;};
in {
  plugins = {
    diffview = {
      enable = true;
      enhancedDiffHl = true;
    };
    fugitive = {
      enable = true;
    };
  };

  extraConfigLua = ''
    function StageAll()
        vim.cmd('G add .')
    end

    function CommitWithMessage()
      local message = vim.fn.input('Commit message: ')
      if message and #message > 0 then
        vim.cmd('G commit -m ' .. vim.fn.shellescape(message))
      else
        print("Commit aborted: empty message")
      end
    end
  '';

  keymaps = [
    {
      key = "<leader>g";
      action = "<leader>g";
      options = {
        desc = "[G]it";
        remap = true;
      };
    }
    {
      key = "<leader>gd";
      action = "<leader>gd";
      options = {
        desc = "[D]iffview";
        remap = true;
      };
    }
    {
      mode = "n";
      key = "<leader>gdo";
      action = "<cmd>DiffviewOpen<CR>";
      options = {
        desc = "[D]iffview [O]pen";
        silent = true;
      };
    }
    {
      mode = "n";
      key = "<leader>gdc";
      action = "<cmd>DiffviewClose<CR>";
      options = {
        desc = "[D]iffview [C]lose";
        silent = true;
      };
    }
    {
      mode = "n";
      key = "<leader>gdf";
      action = "<cmd>DiffviewFileHistory<CR>";
      options = {
        desc = "[D]iffview [F]ile History";
        silent = true;
      };
    }
    {
      mode = "n";
      key = "]h";
      action = helpers.mkRaw "function() require('gitsigns').next_hunk() end";
      options = {
        desc = "Next hunk";
      };
    }
    {
      mode = "n";
      key = "[h";
      action = helpers.mkRaw "function() require('gitsigns').prev_hunk() end";
      options = {
        desc = "Next hunk";
      };
    }
    {
      key = "<leader>gh";
      action = "<leader>gh";
      options = {
        desc = "[H]unks";
        remap = true;
      };
    }
    {
      mode = "n";
      key = "<leader>ghs";
      action = helpers.mkRaw "function() require('gitsigns').stage_hunk() end";
      options = {
        desc = "Stage hunk";
      };
    }
    {
      mode = "n";
      key = "<leader>ghr";
      action = helpers.mkRaw "function() require('gitsigns').reset_hunk() end";
      options = {
        desc = "Reset hunk";
      };
    }
    # TODO: might not work, actual code is:
    # map('v', '<leader>hs', function() gitsigns.stage_hunk {vim.fn.line('.'), vim.fn.line('v')} end)

    {
      mode = "v";
      key = "<leader>ghs";
      action = helpers.mkRaw "function() require('gitsigns').stage_hunk() end";
      options = {
        desc = "Reset hunk";
      };
    }
    # TODO: might not work, actual code is:
    #     map('v', '<leader>hr', function() gitsigns.reset_hunk {vim.fn.line('.'), vim.fn.line('v')} end)
    {
      mode = "v";
      key = "<leader>ghr";
      action = helpers.mkRaw "function() require('gitsigns').reset_hunk() end";
      options = {
        desc = "Reset hunk";
      };
    }
    {
      mode = "n";
      key = "<leader>ghS";
      action = helpers.mkRaw "function() require('gitsigns').stage_buffer() end";
      options = {
        desc = "Stage buffer";
      };
    }
    {
      mode = "n";
      key = "<leader>ghu";
      action = helpers.mkRaw "function() require('gitsigns').undo_stage_hunk() end";
      options = {
        desc = "Undo stage hunk";
      };
    }
    {
      mode = "n";
      key = "<leader>ghp";
      action = helpers.mkRaw "function() require('gitsigns').preview_hunk() end";
      options = {
        desc = "Preview hunk";
      };
    }
    {
      mode = "n";
      key = "<leader>ghd";
      action = helpers.mkRaw "function() require('gitsigns').diffthis() end";
      options = {
        desc = "Diff changes";
      };
    }
    {
      mode = "n";
      key = "<leader>gc";
      action = helpers.mkRaw "function() CommitWithMessage() end";
      options = {
        desc = "Commit with vim-fugitive";
      };
    }
    {
      mode = "n";
      key = "<leader>gC";
      action = helpers.mkRaw "function() StageAll() CommitWithMessage() end";
      options = {
        desc = "Stage all and commit with vim-fugitive";
      };
    }
    {
      mode = "n";
      key = "<C-g>";
      action = helpers.mkRaw "function() StageAll() CommitWithMessage() end";
      options = {
        desc = "Stage all and commit with vim-fugitive";
      };
    }
  ];
}
