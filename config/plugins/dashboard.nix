{
  plugins.dashboard = {
    settings = {
      enable = false;
      config = {
        center = [
          {
            icon = " ";
            desc = "Open File ";
            shortcut = "f";
            action = "Telescope find_files";
          }
          # {
          #   icon = " ";
          #   desc = "Open File ";
          #   shortcut = "g";
          #   action = "Telescope find_files";
          # }
          {
            icon = " ";
            desc = "Open Recent ";
            # shortcut = "r";
            action = "Telescope oldfiles";
          }
          {
            icon = "";
            desc = "Find text ";
            # shortcut = "t";
            action = "Telescope live_grep";
          }
        ];
      };

      hide = {
        statusline = true;
        tabline = true;
      };
    };
  };
}
