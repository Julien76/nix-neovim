{
  plugins = {
    copilot-lua = {
      enable = true;
      settings = {
        panel.enabled = false;
        suggestion.enabled = false;
      };
    };
    copilot-cmp = {
      enable = true;
    };
  };

  keymaps = [
    # {
    #   mode = "n";
    #   key = "<leader>oc";
    #   action = "<cmd>:Copilot panel<cr>";
    #   options = {
    #     silent = true;
    #     desc = "Open Copilot";
    #   };
    # }
    {
      mode = "n";
      key = "<alt>y";
      action = "<cmd>Copilot toggle<cr>";
      options = {
        silent = true;
        desc = "Toggle Copilot";
      };
    }
    {
      mode = "n";
      key = "<leader>uc";
      action = "<cmd>Copilot toggle<cr>";
      options = {
        silent = true;
        desc = "Toggle Copilot";
      };
    }
  ];
}
