{pkgs, ...}: {
  plugins.telescope = {
    enable = true;
    extensions = {
      fzf-native.enable = true;
      ui-select.enable = true; # replaces the vim.ui.select with telescope
      frecency.enable = true;
      frecency.settings = {
        matcher = "fuzzy";
        show_scores = true;
        show_filter_column = false;
        disable_devicons = false;
        ignore_patterns = [
          "*.git/*"
          "*.venv/*"
          "*.direnv/*"
          "*.devenv/*"
          "*.mypy_cache/*"
          "*.node_modules/*"
          "*.pytest_cache/*"
          "*.__pycache__/*"
          "*/tmp/*"
        ];
      };
    };
    settings = {
      defaults = {
        file_ignore_patterns = [
          "^.git/"
          "^.mypy_cache/"
          "^.venv/"
          "^.devenv/"
          "^.direnv/"
          "^__pycache__/"
          "^output/"
          "^data/"
          "^node_modules/"
          "%.ipynb"
        ];
        path_display = {
          filename_first = {
            reverse_directories = true;
          };
        };
        mappings = {
          n = {
            "d" = "delete_buffer";
            "q" = "close";
          };
        };
        # mappings = {
        #   n = {
        #     "d" = require("telescope.actions").delete_buffer,
        #     -- I'm also used to quitting bufexplorer with q instead of escape
        #     ["q"] = require("telescope.actions").close,
        #   },
        # },
        dynamic_preview_title = true; # necessary for neoclip previews
        layout_strategy = "horizontal";
        # layout_config = {
        #   prompt_position = "bottom";
        #   vertical = {width = 0.6;};
        #   horizontal = {
        #     prompt_position = "top";
        #     width = {padding = 0;};
        #     height = {padding = 0;};
        #     preview_width = 0.5;
        #   };
        # };
        sorting_strategy = "ascending";
        # layout_strategy = "flex";
        layout_config = {
          prompt_position = "bottom";
          bottom_pane = {
            height = "80%";
          };
          horizontal = {
            size = {
              width = "90%";
              height = "80%";
            };
          };
          vertical = {
            size = {
              width = "90%";
              height = "90%";
            };
          };
        };
      };
      pickers = {
        colorscheme.enable_preview = true;
        find_files.hidden = true;
        find_files = {
          theme = "ivy";
          layout_config = {height = 0.8;};
        };
        buffers = {
          theme = "ivy";
          layout_config = {height = 0.6;};
        };
        marks = {theme = "dropdown";};
        lsp_document_symbols = {
          theme = "ivy";
          layout_config = {
            height = 0.99;
          };
        };
        git_commits = {
          theme = "ivy";
          layout_config = {
            height = 0.99;
          };
        };
        live_grep = {theme = "ivy";};
        current_buffer_fuzzy_find = {theme = "ivy";};
        keymaps = {theme = "dropdown";};
        vim_options = {theme = "dropdown";};
        commands = {theme = "dropdown";};
      };
    };
    keymaps = {
      "<leader>?" = {
        action = "oldfiles";
        options.desc = "Recent Files (Telescope)";
      };
      "<leader><space>" = {
        # action = "find_files";
        action = "frecency workspace=CWD theme=ivy layout_config={height=0.99}";
        options.desc = "Find files (Telescope)";
      };
      "<leader>," = {
        # action = "buffers";
        action = "buffers sort_mru=true sort_lastused=true initial_mode=normal";
        options.desc = "Buffers (Telescope)";
      };
      "<leader>/" = {
        action = "live_grep";
        options.desc = "Search text (Grep, Telescope)";
      };
      "<leader>gf" = {
        action = "git_files";
        options.desc = "[G]it [F]iles (Telescope)";
      };
      "<leader>sg" = {
        action = "git_commits";
        options.desc = "[G]it [C]ommit (Telescope)";
      };
      "<leader>sf" = {
        action = "find_files";
        options.desc = "[S]earch [F]iles (Telescope)";
      };
      "<leader>sh" = {
        action = "help_tags";
        options.desc = "[S]earch [H]elp (Telescope)";
      };
      "<leader>sw" = {
        action = "grep_string";
        options.desc = "[S]earch [W]ord (Telescope)";
      };
      # "<leader>sg" = {
      #   action = "live_grep";
      #   options.desc = "[S]earch [G]rep (Telescope)";
      # };
      "<leader>sd" = {
        action = "diagnostics";
        options.desc = "[S]earch [D]iagnostics (Telescope)";
      };
      "<leader>sr" = {
        action = "resume";
        options.desc = "[S]earch [R]esume (Telescope)";
      };
      "<leader>ss" = {
        action = "lsp_document_symbols initial_mode=normal";
        options.desc = "[S]earch [S]ymbol (Telescope)";
      };
      "gr" = {
        action = "lsp_references";
        options.desc = "[G]o [R]eferences (Telescope)";
      };
      "gd" = {
        action = "lsp_definitions";
        options.desc = "[G]o [D]efinitions (Telescope)";
      };
    };
  };
  keymaps = [
    {
      key = "<leader>s";
      action = "<leader>s";
      options = {
        desc = "[S]earch (Telescope)";
        remap = true;
      };
    }
  ];
}
