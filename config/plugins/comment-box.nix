{
  lib,
  config,
  ...
}: {
  plugins = {
    comment-box = {
      enable = true;

      settings = {
        # TODO: customize
        #   borders = {
        #     bottom_left = "X";
        #     bottom_right = "X";
        #     top_left = "X";
        #     top_right = "X";
        #   };
        #   box_width = 120;
        #   comment_style = "block";
        #   doc_width = 100;
        #   inner_blank_lines = true;
        #   line_width = 40;
        #   lines = {
        #     line = "*";
        #   };
        #   outer_blank_lines_below = true;
      };
    };

    which-key.settings.spec = lib.optionals config.plugins.comment-box.enable [
      {
        __unkeyed = "<leader>cc";
        group = "[C]ode [C]omment";
        icon = " ";
      }
    ];
  };

  keymaps = lib.mkIf config.plugins.comment-box.enable [
    {
      mode = "n";
      key = "<leader>ccd";
      action = "<cmd>CBd<cr>";
      options = {
        desc = "Delete a box";
      };
    }
    {
      mode = "n";
      key = "<leader>ccc";
      action = "<cmd>CBccbox<cr>";
      options = {
        desc = "Box Title";
      };
    }
    {
      mode = "n";
      key = "<leader>ccl";
      action = "<cmd>CBlcbox<cr>";
      options = {
        desc = "Box Title (left)";
      };
    }
    {
      mode = "n";
      key = "<leader>cct";
      action = "<cmd>CBllline<cr>";
      options = {
        desc = "Titled Line";
      };
    }
    {
      mode = "n";
      key = "<leader>ccs";
      action = "<cmd>CBline<cr>";
      options = {
        desc = "Simple Line";
      };
    }
  ];
}
