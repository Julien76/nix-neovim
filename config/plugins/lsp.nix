{
  pkgs,
  lib,
  ...
}: let
  flakeRoot = "/home/julien/.dotfiles/";
  # flakeRoot = lib.custom.relativeToRoot "./.";
in {
  config.plugins.lsp = {
    enable = true;
    preConfig = ''
      -- Change the Diagnostic symbols in the sign column (gutter)
      local signs = { Error = " ", Warn = " ", Hint = "󰠠 ", Info = " " }
      for type, icon in pairs(signs) do
          local hl = "DiagnosticSign" .. type
          vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
      end


      vim.diagnostic.config {
          virtual_text = true,
          float = {
              header = false,
              border = 'rounded',
              focusable = true,
          }
      }

      vim.lsp.handlers['textDocument/hover'] = vim.lsp.with( vim.lsp.handlers.hover, {border = 'rounded'})
      vim.lsp.handlers['textDocument/signatureHelp'] = vim.lsp.with( vim.lsp.handlers.signature_help, {border = 'rounded'})
    '';
    keymaps = {
      silent = true;
      diagnostic = {
        "<leader>k" = "goto_prev";
        "<leader>j" = "goto_next";
      };
      lspBuf = {
        # "gd" = "definition";
        "gD" = "declaration";
        "gi" = "implementation";
        # "gr" = "references";
        "gt" = "type_definition";
        "K" = "hover";

        # "<C-k>" = "signature_help";

        "<leader>ca" = "code_action";
        "<leader>cr" = "rename";
        # "<leader>wa" = "add_workspace_folder";
        # "<leader>wr" = "remove_workspace_folder";
      };
    };
    # onAttach = ''
    #   vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
    # '';

    servers = {
      lua_ls = {
        enable = true;
        settings = {
          workspace.checkThirdParty = true;
          telemetry.enable = false;
        };
      };
      nil_ls.enable = true;
      # nixd = {
      #   enable = true;
      #   settings = {
      #     # nixpkgs = {
      #     #   expr = "import <nixpkgs> {}";
      #     # };
      #     formatting = {
      #       command = ["nixfmt"];
      #     };
      #     nixpkgs.expr = "import (builtins.getFlake \"~/.dotfiles\").inputs.nixpkgs { }";
      #     options = {
      #       nixos.expr = "(builtins.getFlake \"~/.dotfiles\").nixosConfigurations.oak.options";
      #       darwin.expr = "(builtins.getFlake \"~/.dotfiles\").darwinConfigurations.macbook-air.options";
      #       home-manager.expr = "(builtins.getFlake \"~/.dotfiles\").homeConfigurations.\"julien@oak\".options";
      #     };
      #   };
      # };
      nixd = {
        # Nix LS
        enable = true;
        settings = let
          flake = ''(builtins.getFlake "/home/julien/.dotfiles")'';
          system = ''''${builtins.currentSystem}'';
        in {
          nixpkgs.expr = "import ${flake}.inputs.nixpkgs { }";
          options = {
            flake-parts.expr = "${flake}.debug.options";
            nixos.expr = "${flake}.nixosConfigurations.oak.options";
            home-manager.expr = "${flake}.homeConfigurations.\"julien@oak\".options";
            nixvim.expr = "${flake}.packages.${system}.nvim.options";
          };
          diagnostic = {
            # Suppress noisy warnings
            suppress = [
              "sema-escaping-with"
              "var-bind-to-this"
            ];
          };
        };
      };
      pylyzer = {
        enable = false;
      };
      pyright = {
        enable = true;
        # package = pkgs.basedpyright;
        settings = {
          python = {
            analysis = {
              venvPath = ".";
              venv = ".venv";
              exclude = [
                ".direnv"
                ".devenv"
                ".venv"
                ".vite"
                ".git"
                "**/.direnv"
                "**/.devenv"
                "**/.venv"
                "**/.vite"
                "**/.git"
                "**/*.direnv"
                "**/*.devenv"
                "**/*.venv"
                "**/*.vite"
                "**/*.git"
                "docker"
                "frontend"
                "node_modules"
                "**/node_modules"
                "**/__pycache__"
                "**/.mypy_cache"
                "**/.pytest_cache"
                ".mypy_cache"
                ".pytest_cache"
                "build"
                "dist"
              ];

              autoSearchPaths = true;
              diagnosticMode = "openFilesOnly";
              useLibraryCodeForTypes = true;
              typeCheckingMode = "off";
              diagnosticSeverityOverrides = {
                reportUnusedClass = "none";
                reportUnusedVariable = "none";
                reportFunctionMemberAccess = "none";
                reportUnusedImport = "none";
              };
            };
          };
        };
        extraOptions = {
          settings = {
            python = {
              analysis = {
                venvPath = ".";
                venv = ".venv";
                exclude = [
                  ".direnv"
                  ".devenv"
                  ".venv"
                  ".vite"
                  ".git"
                  "**/.direnv"
                  "**/.devenv"
                  "**/.venv"
                  "**/.vite"
                  "**/.git"
                  "**/*.direnv"
                  "**/*.devenv"
                  "**/*.venv"
                  "**/*.vite"
                  "**/*.git"
                  "docker"
                  "frontend"
                  "node_modules"
                  "**/node_modules"
                  "**/__pycache__"
                  "build"
                  "dist"
                ];
                autoSearchPaths = true;
                diagnosticMode = "openFilesOnly";
                useLibraryCodeForTypes = true;
                typeCheckingMode = "off";
                diagnosticSeverityOverrides = {
                  reportUnusedClass = "none";
                  reportUnusedVariable = "none";
                  reportFunctionMemberAccess = "none";
                  reportUnusedImport = "none";
                };
              };
            };
          };
        };
        # rootDir = ''require("lspconfig").util.root_pattern("pyproject.toml")'';
        rootDir = ''require("lspconfig").util.root_pattern(".git")'';
      };
      # rnix-lsp.enable = true;
      ruff_lsp.enable = true;
      tailwindcss = {
        enable = true;
        filetypes = [
          "css"
          "scss"
          "sass"
          "html"
          "javascript"
          "javascriptreact"
          "typescript"
          "typescriptreact"
          "htmldjango"
          "python"
        ];
        settings = {
          includeLanguages = {
            typescript = "javascript";
            typescriptreact = "javascript";
            htmldjango = "html";
            python = "html";
          };
          tailwindCSS = {
            experimental = {
              classRegex = [
                # Single line strings:
                [''class_="([^"]*)''] # for double quoted strings
                [''class_='([^']*)''] # for single quoted strings
                # Multi-line strings:
                [''class_="""([^"]*)"""''] # for multi-line double quoted strings
                [''class_='''([^']*)'''''] # for multi-line single quoted strings
              ];
            };
          };
        };
      };
      eslint = {
        enable = true;
        extraOptions = {
          settings = {
            autoFixOnSave = true;
          };
        };
        onAttach.function = ''
          client.server_capabilities.documentFormattingProvider = true
          client.server_capabilities.documentRangeFormattingProvider = true

          vim.api.nvim_create_autocmd("BufWritePre", {
            buffer = bufnr,
            command = "EslintFixAll",
          })
        '';
      };
      ts_ls = {
        enable = true;
        extraOptions = {single_file_support = false;};
        rootDir = ''require('lspconfig').util.root_pattern("package.json")'';
      };
      cssls = {
        enable = true;
      };
    };
  };
}
