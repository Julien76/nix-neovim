let
  helpers.mkRaw = r: {__raw = r;};
in {
  plugins = {
    dap = {
      enable = true;

      signs = {
        dapStopped = {
          text = "󰁕 ";
          texthl = "DiagnosticWarn";
          linehl = "DapStoppedLine";
          numhl = "DapStoppedLine";
        };
        dapBreakpoint.text = " ";
        dapBreakpointCondition.text = " ";
        dapBreakpointRejected = {
          text = " ";
          texthl = "DiagnosticError";
        };
        dapLogPoint.text = ".>";
      };
    };

    dap-ui = {
      enable = true;
      settings.render.max_type_length = 0;
    };
    dap-python.enable = true;
    dap-virtual-text.enable = true;

    # which-key = {
    #   registrations = {
    #     "<leader>d" = {name = "+debug";};
    #   };
    # };
  };

  highlight = {
    DapStoppedLine = {
      default = true;
      link = "Visual";
    };
  };

  extraConfigLua = ''
    local dap = require("dap")
    local dapui = require("dapui")
    dap.listeners.after.event_initialized["dapui_config"] = function()
      dapui.open({})
    end
    dap.listeners.before.event_terminated["dapui_config"] = function()
      dapui.close({})
    end
    dap.listeners.before.event_exited["dapui_config"] = function()
      dapui.close({})
    end
  '';

  keymaps = [
    {
      key = "<leader>d";
      action = "<leader>d";
      options = {
        desc = "[D]ebug";
        remap = true;
      };
    }
    {
      key = "<leader>de";
      action = helpers.mkRaw ''
        function() require("dapui").eval() end
      '';
      options.desc = "Eval";
    }

    {
      key = "<leader>dB";
      action = helpers.mkRaw ''
        function() require("dap").set_breakpoint(vim.fn.input('Breakpoint condition: ')) end
      '';
      options.desc = "Breakpoint with Condition";
    }

    {
      key = "<leader>db";
      action = helpers.mkRaw ''
        function() require("dap").toggle_breakpoint() end
      '';
      options.desc = "Toggle Breakpoint";
    }

    {
      key = "<leader>dc";
      action = helpers.mkRaw ''
        function() require("dap").continue() end
      '';
      options.desc = "Continue";
    }

    {
      key = "<leader>da";
      action = let
        get_args = ''
          (function (config)
            local args = type(config.args) == "function" and (config.args() or {}) or config.args or {}
            config = vim.deepcopy(config)
            ---@cast args string[]
            config.args = function()
              local new_args = vim.fn.input("Run with args: ", table.concat(args, " ")) --[[@as string]]
              return vim.split(vim.fn.expand(new_args) --[[@as string]], " ")
            end
            return config
          end)
        '';
      in
        helpers.mkRaw ''
          function() require("dap").continue({ before = ${get_args}}) end
        '';
      options.desc = "Run with Args";
    }

    {
      key = "<leader>dC";
      action = helpers.mkRaw ''
        function() require("dap").run_to_cursor() end
      '';
      options.desc = "Run to Cursor";
    }

    {
      key = "<leader>dg";
      action = helpers.mkRaw ''
        function() require("dap").goto_() end
      '';
      options.desc = "Go to Line (no execute)";
    }

    {
      key = "<leader>di";
      action = helpers.mkRaw ''
        function() require("dap").step_into() end
      '';
      options.desc = "Step Into";
    }

    {
      key = "<leader>dj";
      action = helpers.mkRaw ''
        function() require("dap").down() end
      '';
      options.desc = "Down";
    }

    {
      key = "<leader>dk";
      action = helpers.mkRaw ''
        function() require("dap").up() end
      '';
      options.desc = "Up";
    }

    {
      key = "<leader>dl";
      action = helpers.mkRaw ''
        function() require("dap").run_last() end
      '';
      options.desc = "Run Last";
    }

    {
      key = "<leader>do";
      action = helpers.mkRaw ''
        function() require("dap").step_out() end
      '';
      options.desc = "Step Out";
    }

    {
      key = "<leader>dO";
      action = helpers.mkRaw ''
        function() require("dap").step_over() end
      '';
      options.desc = "Step Over";
    }

    {
      key = "<leader>dp";
      action = helpers.mkRaw ''
        function() require("dap").pause() end
      '';
      options.desc = "Pause";
    }

    {
      key = "<leader>dr";
      action = helpers.mkRaw ''
        function() require("dap").repl.toggle() end
      '';
      options.desc = "Toggle REPL";
    }

    {
      key = "<leader>ds";
      action = helpers.mkRaw ''
        function() require("dap").session() end
      '';
      options.desc = "Session";
    }

    {
      key = "<leader>dt";
      action = helpers.mkRaw ''
        function() require("dapui").terminate() end
      '';
      options.desc = "Terminate";
    }

    {
      key = "<leader>dw";
      action = helpers.mkRaw ''
        function() require("dap.ui.widgets").hover() end
      '';
      options.desc = "Widgets";
    }

    {
      key = "<leader>du";
      action = helpers.mkRaw ''
        function() require("dapui").toggle() end
      '';
      options.desc = "Dap UI";
    }

    {
      key = "<leader>de";
      action = helpers.mkRaw ''
        function()
          require("dapui").eval()
        end
      '';
      mode = ["n" "v"];
      options.desc = "Eval";
    }

    {
      key = "<leader>dd";
      action = helpers.mkRaw ''
        function()
          local len = require("dapui.config").render.max_type_length
          len = (len == -1) and 0 or -1
          require("dapui").update_render({ max_type_length = len })
        end
      '';
      options.desc = "Toggle Rendering of Types";
    }
  ];
}
