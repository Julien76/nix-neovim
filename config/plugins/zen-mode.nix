{
  plugins.zen-mode = {
    enable = true;
    settings = {
      window = {
        backdrop = 1.00;
        height = 0.85;
        width = 140; # uncomment any of the options below, or add other vim.wo options you want to apply
        options = {
          signcolumn = "yes"; # disable signcolumn
          number = false; # disable number column
          relativenumber = false; # disable relative numbers
          # cursorline = false; # disable cursorline
          # cursorcolumn = false; # disable cursor column
          # foldcolumn = "0"; # disable fold column
          # list = false; # disable whitespace characters
        };
      };
      plugins = {
        options = {
          enabled = true;
          ruler = false; # disables the ruler text in the cmd line area
          showcmd = false; # disables the command in the last line of the screen
        };
        twilight = {enabled = true;}; # enable to start Twilight when zen mode opens
        gitsigns = {enabled = false;}; # disables git signs
        tmux = {enabled = true;}; # disables the tmux statusline
        # this will change the font size on kitty when in zen mode
        # to make this work; you need to set the following kitty options:
        # - allow_remote_control socket-only
        # - listen_on unix:/tmp/kitty
        kitty = {
          enabled = true;
          font = "+4"; # font size increment
        };
      };
      on_open = ''
        function(win)
        	require("noice").cmd("disable")
          -- vim.diagnostic.config({virtual_text = false})
          -- vim.cmd('highlight ColorColumn guibg=#000000');
        	-- lualine.hide({})
        	vim.opt.laststatus = 2
        	vim.o.statusline = ""
        	-- vim.o.cmdheight = 1
        end
      '';
      on_close = ''
        function(win)
          -- vim.diagnostic.config({virtual_text = true})
          require("noice").cmd("enable")
          -- vim.o.cmdheight = 0
          -- lualine.hide({ unhide = true })
        end
      '';
    };
  };

  keymaps = [
    {
      mode = "n";
      key = "<leader>z";
      action = "<cmd>lua require('zen-mode').toggle()<cr>";
      options = {
        desc = "Zen mode";
      };
    }
  ];
}
