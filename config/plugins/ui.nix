{
  plugins = {
    web-devicons = {
      enable = true;
    };
    bufferline = {
      enable = false;
    };
    lualine = {
      enable = true;
      settings = {
        globalstatus = true;
        icons_enabled = true;
        extensions = ["nvim-tree"];
      };
    };
    barbecue = {
      enable = true;
      settings = {
        attach_navic = true;
        modified = "function(bufnr) return vim.bo[bufnr].modified end";
      };
    };
    navbuddy = {
      enable = true;
      lsp = {
        autoAttach = true;
      };
    };
    navic = {
      enable = true;
      settings = {
        click = true;
        highlight = true;
        lazy_update_context = true;
        lsp = {
          auto_attach = true;
          preference = ["typescript" "html" "tailwind"];
        };
      };
    };
  };

  keymaps = [
    {
      mode = "n";
      key = "<leader>nn";
      action = "<cmd>Navbuddy<cr>";
      options = {
        desc = "Navigation via Navbuddy";
      };
    }
  ];
}
