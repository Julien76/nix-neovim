let
  helpers.mkRaw = r: {__raw = r;};
in {
  plugins = {
    mini = {
      enable = true;
      modules = {
        ai = {};
        surround = {
          mappings = {
            add = "gsa"; ## Add surrounding in Normal and Visual modes
            delete = "gsd"; ## Delete surrounding
            find = "gsf"; ## Find surrounding (to the right)
            find_left = "gsF"; ## Find surrounding (to the left)
            highlight = "gsh"; ## Highlight surrounding
            replace = "gsr"; ## Replace surrounding
            update_n_lines = "gsn"; ## Update `n_lines`
          };
        };
        # comment = {
        #   options = {
        #     customCommentString = ''
        #       <cmd>lua require("ts_context_commentstring.internal").calculate_commentstring() or vim.bo.commentstring<cr>
        #     '';
        #   };
        # };
        cursorword = {};
        bracketed = {};
        pairs = {};
        bufremove = {};
        indentscope = {
          symbol = "│";
          options = {try_as_border = true;};
        };
        # sessions = {
        #   autoread = false;
        #   autowrite = false;
        #   file = "";
        #   directory.__raw = "vim.fn.stdpath('state')..'/sessions/'";
        # };
        starter = {
          # header = lib.concatLines [
          #   "███╗   ██╗██╗██╗  ██╗██╗   ██╗██╗███╗   ███╗"
          #   "████╗  ██║██║╚██╗██╔╝██║   ██║██║████╗ ████║"
          #   "██╔██╗ ██║██║ ╚███╔╝ ██║   ██║██║██╔████╔██║"
          #   "██║╚██╗██║██║ ██╔██╗ ╚██╗ ██╔╝██║██║╚██╔╝██║"
          #   "██║ ╚████║██║██╔╝ ██╗ ╚████╔╝ ██║██║ ╚═╝ ██║"
          #   "╚═╝  ╚═══╝╚═╝╚═╝  ╚═╝  ╚═══╝  ╚═╝╚═╝     ╚═╝"
          # ];
          header = "";
          items = [
            {
              name = "Find File";
              action = "Telescope find_files follow=true no_ignore=true hidden=true";
              section = "";
            }
            {
              name = "Recent File";
              action = "Telescope oldfiles";
              section = "";
            }
            {
              name = "Last session CWD";
              action = helpers.mkRaw "function() require('persistence').load() end";
              section = "";
            }
            {
              name = "All sessions";
              action = helpers.mkRaw "function() require('persistence').select() end";
              section = "";
            }
            # {
            #   name = "Copilot";
            #   action = "CopilotChat";
            #   section = "";
            # }
            # {
            #   name = "ChatGPT";
            #   action = "ChatGPT";
            #   section = "";
            # }
            {
              name = "Quit";
              action = "qa";
              section = "";
            }
          ];
          footer = "";
        };
      };
    };
  };
}
