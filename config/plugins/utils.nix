{
  plugins = {
    oil.enable = true;
    undotree.enable = true;
    fugitive.enable = true;
    nvim-tree.enable = true;
    tmux-navigator.enable = true;
    todo-comments.enable = true;
    tailwind-tools.enable = true;

    auto-save = {
      enable = true;
      settings = {
        debounce_delay = 300000; # 5 minutes
        write_all_buffers = true;
      };
    };

    gitsigns = {
      enable = true;
    };
    noice = {
      enable = true;
      settings.lsp.signature.enabled = false;
    };
    trouble = {
      enable = true;
    };
    persistence = {
      enable = true;
    };
    # lspsaga = {
    #   enable = true;
    # };
    # wilder = {
    #   enable = true;
    #   modes = [":" "/" "?"];
    # };
    which-key = {
      enable = true;
      settings.show_keys = true;
      # registrations = {
      #   "<leader>b" = "+buffer";
      #   "<leader>s" = "+search";
      #   # "<leader>f" = "File";
      #   # "<leader><localleader>" = "Local";
      # };
    };
    illuminate = {
      enable = true;
      delay = 200;
    };
    indent-blankline = {
      enable = true;
      settings = {
        indent = {
          char = "│";
          tab_char = "│";
        };
        scope = {enabled = false;};
        exclude = {
          filetypes = [
            "help"
            "alpha"
            "dashboard"
            "neo-tree"
            "Trouble"
            "trouble"
            "lazy"
            "mason"
            "notify"
            "toggleterm"
            "lazyterm"
          ];
        };
      };
    };
    comment = {
      enable = true;
      settings.pre_hook =
        /*
        lua
        */
        ''
          require('ts_context_commentstring.integrations.comment_nvim').create_pre_hook()
        '';
    };
  };
}
