let
  helpers.mkRaw = r: {__raw = r;};
in {
  plugins.toggleterm = {
    enable = true;
    # settings.open_mapping = "[[C-/>]]";
  };

  keymaps = [
    {
      key = "<leader>ft";
      action = helpers.mkRaw ''
        function()
          local term = require("toggleterm.terminal").Terminal:new({
            direction = "float",
            dir = Root(),
          })
          term:toggle()
        end
      '';
      options.desc = "Terminal (root dir)";
    }
    {
      key = "<leader>fT";
      action = helpers.mkRaw ''
        function()
          local term = require("toggleterm.terminal").Terminal:new({
            direction = "float",
            dir = vim.loop.cwd(),
          })
          term:toggle()
        end
      '';
      options.desc = "Terminal (cwd)";
    }
    {
      key = "<C-/>";
      action = helpers.mkRaw ''
        function()
          local term = require("toggleterm.terminal").Terminal:new({
            direction = "float",
            dir = Root(),
          })
          term:toggle()
        end
      '';
      options.desc = "Terminal (root dir)";
    }
    {
      key = "<C-_>";
      action = helpers.mkRaw ''
        function()
          local term = require("toggleterm.terminal").Terminal:new({
            direction = "float",
            dir = Root(),
          })
          term:toggle()
        end
      '';
      options.desc = "whech_key_ignore";
    }
    {
      key = "<leader>gg";
      action = helpers.mkRaw ''
        function()
          local term = require("toggleterm.terminal").Terminal:new({
            direction = "float",
            dir = Root(),
            cmd = "lazygit",
          })
          term:toggle()
        end
      '';
      options.desc = "Lazygit (rootdir)";
    }
    {
      key = "<leader>gG";
      action = helpers.mkRaw ''
        function()
          local term = require("toggleterm.terminal").Terminal:new({
            direction = "float",
            dir = vim.loop.cwd(),
            cmd = "lazygit",
          })
          term:toggle()
        end
      '';
      options.desc = "Lazygit (cwd)";
    }
  ];
}
