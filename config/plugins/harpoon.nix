{
  lib,
  pkgs,
  ...
}: let
  helpers.mkRaw = r: {__raw = r;};
in {
  plugins = {
    harpoon = {
      enable = true;
      markBranch = true;
      package = pkgs.vimPlugins.harpoon2;
      # keymaps = {
      #   navFile = {
      #     "1" = "<A-1>";
      #     "2" = "<A-2>";
      #     "3" = "<A-3>";
      #   };
      #   addFile = "<leader>a";
      #   toggleQuickMenu = "<leader>h";
      # };
    };
    which-key.registrations = lib.listToAttrs (builtins.genList (i: {
        name = "<leader>${toString i}";
        value = "which_key_ignore";
      })
      10);
  };
  keymaps = let
    fn = str:
      helpers.mkRaw ''
        function()
            local harpoon = require("harpoon")
            ${str}
        end'';
  in
    lib.flatten [
      {
        key = "<leader>ha";
        action = fn "harpoon:list():append()";
        options.desc = "Harpoon Add";
      }
      {
        key = "<leader>hh";
        action = fn "harpoon.ui:toggle_quick_menu(harpoon:list())";
        options.desc = "Harpoon Menu";
      }
      {
        key = "<leader>h";
        action = "<leader>h";
        options = {
          desc = "[H]arpoon";
          remap = true;
        };
      }
      (builtins.genList (i: {
          key = "<leader>${toString i}";
          action = fn "harpoon:list():select(${toString i})";
        })
        10)
    ];
}
