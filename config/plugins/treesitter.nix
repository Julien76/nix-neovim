{
  plugins = {
    treesitter = {
      enable = true;
      settings.indent.enable = true;
      nixvimInjections = true;
      # ensureInstalled = [
      #   "bash"
      #   "css"
      #   "latex"
      #   "lua"
      #   "make"
      #   "markdown"
      #   "markdown_inline"
      #   "nix"
      #   "python"
      #   "regex"
      # ];
      settings.incremental_selection = {
        enable = true;
        keymaps = {
          init_selection = "<TAB>";
          node_incremental = "<TAB>";
          node_decremental = "<bs>";
        };
      };
    };

    treesitter-context = {
      enable = true;
      settings.max_lines = 3;
    };

    treesitter-textobjects = {
      enable = true;
      move = {
        enable = true;
        gotoNextStart = {
          "]f" = "@function.outer";
          "]c" = "@class.outer";
          # "]m" = "@function.outer";
          # "]]" = "@class.outer";
        };

        gotoNextEnd = {
          "]F" = "@function.outer";
          "]C" = "@class.outer";
          # "]M" = "@function.outer";
          # "][" = "@class.outer";
        };

        gotoPreviousStart = {
          "[f" = "@function.outer";
          "[c" = "@class.ouver";
          # "[m" = "@function.outer";
          # "[[" = "@class.outer";
        };

        gotoPreviousEnd = {
          "[F" = "@function.outer";
          "[C" = "@class.outer";
          # "[M" = "@function.outer";
          # "[]" = "@class.outer";
        };
      };
      swap = {
        enable = true;
        swapNext = {
          "<leader>an" = "@parameter.inner";
        };
        swapPrevious = {
          "<leader>ap" = "@parameter.outer";
        };
      };
    };

    ts-autotag = {
      enable = true;
    };

    ts-context-commentstring = {
      enable = true;
      disableAutoInitialization = false;
    };
  };

  keymaps = [
    {
      key = "<leader>a";
      action = "<leader>a";
      options = {
        desc = "[A]rgument text object";
        remap = true;
      };
    }
  ];
}
