{pkgs, ...}: {
  extraPlugins = [
    pkgs.vimPlugins.yazi-nvim
    # (pkgs.vimUtils.buildVimPlugin {
    #   name = "squeel.nvim";
    #   src = pkgs.fetchFromGitHub {
    #     owner = "OleJoik";
    #     repo = "squeel.nvim";
    #     rev = "master";
    #     hash = "sha256-sgCX1JG/x+rQ9OR3W1oFtpAw3RdXCxARcOyddFhHQMw=";
    #   };
    # })
  ];
  extraConfigLua = ''
    require("yazi").setup({
    	-- your configuration
    })
  '';
}
