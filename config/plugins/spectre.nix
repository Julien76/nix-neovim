{pkgs, ...}: let
  helpers.mkRaw = r: {__raw = r;};
in {
  extraPlugins = [
    pkgs.vimPlugins.nvim-spectre
  ];

  extraConfigLua = ''
    require('spectre').setup({
      open_cmd = "noswapfile vnew"
    })
  '';

  keymaps = [
    {
      key = "<leader>sr";
      action = helpers.mkRaw ''
        require('spectre').open
      '';
      options.desc = "[S]earch and [R]eplace in files (Spectre)";
    }
  ];
}
