{
  autoGroups = {
    highlight_yank = {};
    # wscleanup = {};
    resize_splits = {};
    close_with_q = {};
    wrap_spell = {};
  };

  autoCmd = [
    # Highlight yanked text
    {
      group = "highlight_yank";
      event = "TextYankPost";
      pattern = "*";
      callback = {__raw = "function() vim.highlight.on_yank { higroup = 'IncSearch', timeout = 200 } end";};
    }
    # Resize splits if window got resized
    {
      event = "VimResized";
      callback.__raw = ''
        function()
          local current_tab = vim.fn.tabpagenr()
          vim.cmd("tabdo wincmd =")
          vim.cmd("tabnext " .. current_tab)
        end
      '';
      group = "resize_splits";
    }
    # Close some filetypes with <q>
    {
      event = "FileType";
      pattern = [
        "PlenaryTestPopup"
        "help"
        "lspinfo"
        "man"
        "notify"
        "qf"
        "query"
        "spectre_panel"
        "startuptime"
        "tsplayground"
        "neotest-output"
        "checkhealth"
        "neotest-summary"
        "neotest-output-panel"
      ];
      callback.__raw = ''
        function(event)
          vim.bo[event.buf].buflisted = false
          vim.keymap.set("n", "q", "<cmd>close<cr>", { buffer = event.buf, silent = true })
        end
      '';
      group = "close_with_q";
    }
    # Wrap and check for spell in text filetypes
    {
      event = "FileType";
      pattern = [
        "gitcommit"
        "markdown"
        "neorg"
      ];
      callback.__raw = ''
        function()
          vim.opt_local.wrap = true
          vim.opt_local.spell = true
        end
      '';
      group = "wrap_spell";
    }
    # {
    #   group = "wscleanup";
    #   event = "BufWritePre";
    #   pattern = "*";
    #   command = "%s/\\s\\+$//e";
    # }

    # Vertically center document when entering insert mode
    # {
    #   event = "InsertEnter";
    #   command = "norm zz";
    # }

    # Remove trailing whitespace on save
    # {
    #   event = "BufWrite";
    #   command = "%s/\\s\\+$//e";
    # }

    # # Highlight yanked text
    # {
    #   event = "TextYankPost";
    #   group = "highlight_yank";
    #   command = ''lua vim.highlight.on_yank { higroup='IncSearch', timeout=200 }'';
    # }

    # Open help in a vertical split
    {
      event = "FileType";
      pattern = "help";
      command = "wincmd L";
    }

    # Set indentation to 2 spaces for nix files
    # {
    #   event = "FileType";
    #   pattern = "nix";
    #   command = "setlocal tabstop=2 shiftwidth=2";
    # }

    # Enable spellcheck for some filetypes
    {
      event = "FileType";
      pattern = [
        "tex"
        "latex"
        "markdown"
      ];
      command = "setlocal spell spelllang=en,fr,no";
    }
  ];
}
